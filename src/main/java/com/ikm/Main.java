package com.ikm;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Set;

/**
 * @author wangjf
 * @date 2018/8/26 0026.
 */
public class Main {

    static {
        System.out.println("static");
    }

    {
        System.out.println("not static");
    }

    public static void main(String[] args) {

        FinalClass finalClass = new FinalClass();
        finalClass.setNum(2);
        System.out.println(finalClass.getNum());

        finalClass.setNum(3);
        System.out.println(finalClass.getNum());

//        System.out.println(String.format("Local time: %1$tB", Calendar.getInstance()));
////        System.out.println(String.format("Local time: %1$", Calendar.getInstance()));
////        System.out.println(String.format("Local time: %tT", Calendar.getInstance().toString()));
//        System.out.println(String.format("Local time: %tH:%tM:%tS", Calendar.getInstance(), Calendar.getInstance(),Calendar.getInstance()));
//        System.out.println(String.format("Local time: %tT", Calendar.getInstance()));

//        Shape  shape =new Qua();
//        Qua qua = new Qua();
//        shape = qua;
//        Tri tri = (Tri) shape;
//      int x= 0, y=4,z=5;
//      if(x>0)
//          if(y<3)
//              System.out.println("one");
//      else if(y<4)
//              System.out.println("two");
//      else if(z>5)
//              System.out.println("three");
//      else
//              System.out.println("for");
    }

    public static void meth(String[] args){
        throw new RuntimeException();
    }

    public static void TestCollection(MyCollection<?> collection){
        Set set = collection.getCollection();
    }






    public static boolean isLeapYear1(int year){
        Calendar cal = Calendar.getInstance();
        cal.set(year, 1, 1);
        int days = cal.getMaximum(Calendar.DAY_OF_MONTH);
        System.out.println(days);
        return (days == 29);
    }

    public static boolean isLeapYear2(int year){
        GregorianCalendar gc = new GregorianCalendar();
        return gc.isLeapYear(year);
    }

    public static boolean isLeapYear3(int year){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        int days = cal.getActualMaximum(Calendar.DAY_OF_YEAR);
        return (days > 365);
    }

    // wrong
    public boolean isLeapYear4(int year){
        Calendar cal = Calendar.getInstance();
        cal.set(year, Calendar.FEBRUARY, 1);
//        int days = cal.get
        return false;
    }

    // false
    public boolean isLeapYear5(int year){
        Calendar cal = Calendar.getInstance();

        return false;
    }

}
