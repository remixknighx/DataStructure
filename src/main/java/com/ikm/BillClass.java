package com.ikm;

/**
 * @author wangjf
 * @date 2018/8/26 0026.
 */
public final class BillClass implements Cloneable {
    @Override
    protected Object clone() throws CloneNotSupportedException{
        return super.clone();
    }
    public String getInfo(){
        return "MyClass";
    }

    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
//        BillClass myClass = new BillClass();
//        BillClass billClass = (BillClass) BillClass.class.getClassLoader().loadClass("com.ikm.BillClass").newInstance();
//        BillClass billClass = (BillClass) Class.forName("com.ikm.BillClass").newInstance();
//        Class class = Class.forName("com.ikm.BillClass");
//        System.out.println(billClass.getInfo());
    }
}
