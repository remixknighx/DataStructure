package com.ikm;

/**
 * @author wangjf
 * @date 2018/8/26 0026.
 */
public final class FinalClass {

    private int num;

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
