package com.leetcode;

/**
 * @author wangjf
 * @date 2017/7/12.
 */
public class ListNode {

    int val;
    ListNode next;
    ListNode(int x) {
        val = x;
    }
}
